require 'test_helper'

class ResolverTest < Minitest::Test
  def test_it_resolves_20_by_2
    r = CashSession::Resolver.new(:notes, 2, BigDecimal(20.0, 2))
    solutions = r.all
    refute_empty solutions
    assert_equal solutions.length, 1
    assert_equal solutions[0], [BigDecimal(10.0, 2), BigDecimal(10.0, 2)]
  end

  def test_it_resolves_all_by_4
    combinations = []
    CashSession::NOTES_CENTS.each do |v1|
      CashSession::NOTES_CENTS.each do |v2|
        CashSession::NOTES_CENTS.each do |v3|
          CashSession::NOTES_CENTS.each do |v4|
            combinations << [BigDecimal(v1 / 100.0, 2), BigDecimal(v2 / 100.0, 2), BigDecimal(v3 / 100.0, 2), BigDecimal(v4 / 100.0, 2)].sort.reverse
          end
        end
      end
    end

    combinations.uniq.each do |c|
      r = CashSession::Resolver.new(:notes, 4, c.sum)
      t = r.all
      refute_empty(t)
      if t.length == 1
        assert_equal t[0], c
      else
        assert_includes(t, c)
      end
    end
  end
end