module CashSession
  class State
    def initialize
      @state = {
        recyclers: units_hash(UNITS),
        # detailed
        boxes: units_hash(UNITS),
        # totaled
        bulk: { coins: [0, 0], notes: [0, 0] }
      }
      @boxes = { coins: :boxes, notes: :boxes } # could be :boxes or :bulk
      @flagged = []
      @action = nil
      @amount = BigDecimal(0,2)
      @total = BigDecimal(0, 2)
    end

    def update(session)
      update_inventory(session.raw[:before])
      current = value

      case session.nature
      when :in, :pay, :out
        process_in(
          session.raw[:before], session.raw[:after],
          Hash(session.cash_info[:in]), Hash(session.cash_info[:out])
        )
      when :collect
        process_collect(
          session.raw[:before], session.raw[:after]
        )
      when :unlock
        process_unlock(
          session.units,
          session.raw[:before], session.raw[:after]
        )
      else
        raise Unsupported, "Event unsupported: #{session.nature}"
      end

      process_inventory(session.raw[:before], session.raw[:after])

      @timestamp = session.timestamp
      @flagged = session.flagged

      @action = session.nature
      @amount = value - current
    end

    def display
      recyclers = sum_units(@state[:recyclers], UNITS)[1]
      boxes = sum_units(@state[:boxes], UNITS)[1]
      bulk = @state[:bulk][:coins][1] + @state[:bulk][:notes][1]

      <<~END_OF_FORMAT
        State at #{@timestamp.strftime('%y-%m-%d %H:%M')}
        Recyclers: [ #{counts_to_s(@state[:recyclers])} ] = #{'%6.2f' % recyclers}
        Boxes:     [ #{counts_to_s(@state[:boxes])} ] = #{'%6.2f' % boxes}
        Bulk:      [ coins: [ #{bulk_to_s(@state[:bulk][:coins])} ], notes: [ #{bulk_to_s(@state[:bulk][:notes])} ] ] = #{'%6.2f' % bulk}
        Coins:     #{@boxes[:coins]}
        Notes:     #{@boxes[:notes]}
        Flagged:   [ #{units_to_s(@flagged)} ]
      END_OF_FORMAT
    end

    def to_h
      rc = recyclers(:coins)
      rn = recyclers(:notes)

      bc = boxes(:coins)
      bn = boxes(:notes)

      h = {
        timestamp: @timestamp,
        action: @action,
        amount: @amount,

        total: rc[1] + rn[1] + bc[1] + bn[1],

        recyclers_coins_count: rc[0],
        recyclers_coins_value: rc[1],
        recyclers_notes_count: rn[0],
        recyclers_notes_value: rn[1],
        recyclers_count: rc[0] + rn[0],
        recyclers_value: rc[1] + rn[1],

        boxes_coins_count: bc[0],
        boxes_coins_value: bc[1],
        boxes_notes_count: bn[0],
        boxes_notes_value: bn[1],
        boxes_count: bc[0] + bn[0],
        boxes_value: bc[1] + bn[1],

        flagged: @flagged
      }

      UNITS.each do |u|
        h["recycler_#{u}".to_sym] = @state[:recyclers][u]
        h["box_#{u}".to_sym] = @state[:boxes][u]
      end

      h
    end

    def recyclers(unit)
      sum_units(@state[:recyclers], CashSession.units_list(unit))
    end

    def boxes(unit)
      if unit == :all
        [
          @state[:bulk][:coins][0] + @state[:bulk][:notes][0],
          @state[:bulk][:coins][1] + @state[:bulk][:notes][1]
        ]
      elsif @boxes[unit] == :boxes
        sum_units(@state[:boxes], CashSession.units_list(unit))
      else
        @state[:bulk][unit]
      end
    end

    def value
      recyclers(:all)[1] + boxes(:all)[1]
    end

    private

    def process_in(before, after, cash_in, cash_out)
      UNITS.each do |u|
        unit_in  = cash_in.fetch(u, 0)
        unit_out = cash_out.fetch(u, 0)

        next if unit_in.zero? && unit_out.zero?

        recyclers = after[u][:c] - before[u][:c]
        boxes = (unit_in - unit_out) - recyclers

        @state[:recyclers][u] += recyclers
        @state[:boxes][u] += boxes

        bulk = CashSession.bulk_box(u)
        @state[:bulk][bulk][0] += boxes
        @state[:bulk][bulk][1] += boxes * VALUES[u]
      end
    end

    def process_collect(before, after)
      UNITS.each do |u|
        collected = before[u][:c] - after[u][:c]
        next if collected.zero?

        @state[:recyclers][u] -= collected
        @state[:boxes][u]     += collected

        bulk = CashSession.bulk_box(u)
        @state[:bulk][bulk][0] += collected
        @state[:bulk][bulk][1] += collected * VALUES[u]
      end
    end

    def process_unlock(units, before, after)
      units.each do |u|
        b = before[u][:c]
        a = after[u][:c]
        next if b == a

        raise NotEmpty, "Unlock: #{u} box should be empty after unlock" if a != 0

        reset_units(u)
      end
    end

    def update_inventory(before)
      UNITS.each do |u|
        next if @state[:recyclers][u] == before[u][:c]

        @state[:recyclers][u] = before[u][:c]
      end

      BOXES.each do |b|
        bulk = [before[b][:c], before[b][:t]]
        next if @state[:bulk][b] == bulk

        reset_units(b)
        next if bulk[0].zero?

        @state[:bulk][b] = bulk
        @boxes[b] = :bulk
      end
    end

    def process_inventory(before, after)
      diffs = {}
      UNITS.each do |u|
        diffs[u] = after[u][:c] - before[u][:c]
        next if diffs[u].zero? || (@state[:recyclers][u] == after[u][:c])

        @state[:recyclers][u] = after[u][:c]
      end

      BOXES.each do |box|
        next if (before[box] == after[box]) || (@state[:bulk][box] == [after[box][:c], after[box][:t]])

        diff = [after[box][:c] - before[box][:c], after[box][:t] - before[box][:t]]
        r = Resolver.new(box, diff[0], diff[1])
        solutions = r.all
        if solutions.length == 1
          solution = Hash.new(0)
          solutions[0].each_with_object(solution) do |v, a|
            a[VALUES_UNITS[v]] += 1
            a
          end
          solution.each do |u, c|
            @state[:boxes][u] += c
          end
        else
          @boxes[box] = :bulk
        end

        @state[:bulk][box] = [after[box][:c], after[box][:t]]
      end
    end

    def reset_units(unit)
      CashSession.units_list(unit).each { |u| @state[:boxes][u] = 0 }
      @state[:bulk][unit] = [0, 0]
      @boxes[unit] = :boxes
    end

    def sum_units(state, units)
      units.reduce([0, BigDecimal(0, 2)]) do |s, u|
        [
          s[0] + state[u],
          s[1] + state[u] * VALUES[u]
        ]
      end
    end

    # Initialize a Hash where keys are "UNITS" symbols
    def units_hash(units)
      n = 0
      units.each_with_object(n).to_h
    end

    # Returns a formatted string of U: Count
    def counts_to_s(counts)
      counts.map { |u, c| "#{'%05s' % u.to_s}: #{'%3d' % c}" }.join(', ')
    end

    def units_to_s(units)
      units.join(', ')
    end

    def bulk_to_s(bulk)
      "#{bulk[0]}, #{'%.2f' % bulk[1]}"
    end

  end
end