module CashSession

  COINS = %i[p1c p2c p5c p10c p20c p50c p1e p2e].freeze
  NOTES = %i[b5e b10e b20e b50e b100e b200e b500e].freeze
  UNITS = COINS + NOTES
  BOXES = %i[coins notes].freeze

  COINS_CENTS  = [1, 2, 5, 10, 20, 50, 1_00, 2_00].freeze
  NOTES_CENTS  = [5_00, 10_00, 20_00, 50_00, 100_00, 200_00, 500_00].freeze

  COINS_VALUES = Hash[COINS.zip(COINS_CENTS.map { |v| BigDecimal(v / 100.0, 2) })].freeze
  NOTES_VALUES = Hash[NOTES.zip(NOTES_CENTS.map { |v| BigDecimal(v / 100.0, 2) })].freeze
  VALUES       = COINS_VALUES.merge(NOTES_VALUES).freeze

  COINS_UNITS  = Hash[COINS_CENTS.map { |v| BigDecimal(v / 100.0, 2) }.zip(COINS)].freeze
  NOTES_UNITS  = Hash[NOTES_CENTS.map { |v| BigDecimal(v / 100.0, 2) }.zip(NOTES)].freeze
  VALUES_UNITS = COINS_UNITS.merge(NOTES_UNITS).freeze

  def self.units_list(units)
    case units
    when :notes
      NOTES
    when :coins
      COINS
    when :all
      UNITS
    else
      raise UnsupportedUnit, "#{units} is not supported here. Must be :notes or :coins"
    end
  end

  def self.bulk_box(unit)
    COINS.include?(unit) ? :coins : :notes
  end

end
