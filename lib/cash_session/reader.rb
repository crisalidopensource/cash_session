module CashSession

  class Reader
    attr_reader :data

    def initialize(file)
      # Important !! Si les fins de ligne du fichier sont \r\n il faut enlever les \r
      @file = StringScanner.new(File.read(file, encoding: Encoding::UTF_8).tr("\r", ""))
      @data = {}
      decode
    end

    def next_line
      @file.unscan unless @file.getch == "\n"
    end

    def decode
      until @file.eos?
        if @file.scan(/^(\w+): (.*)/).nil?
          next_line
          next
        end

        key = @file[1]
        value = @file[2]

        case value
        when "{"
          scanned = @file.scan(/.*?^}\R/m)
          if scanned
            value += scanned
          else
            raise Error.new('Expected : }')
          end
        when '(*'
          scanned = @file.scan(/.*?^*\)\R/m)
          if scanned
            value += scanned
          else
            raise Error.new('Expected : *)')
          end
        else
          # all right, do nothing
        end
        @data[key] = value
        next_line
      end
    end

    # "" (no session), pay, credit, in, out, collect, control, change, unlock, "" (unknown)
    def nature
      nature = @data['nature']
      nature == '' ? :no_session : nature.to_sym
    end

    # timestamp ISO8601
    def timestamp
      @timestamp ||= DateTime.parse(@data['timestamp'])
    end

    # transaction amount
    def amount
      @amount ||= BigDecimal(@data['amount'], 2)
    end

    # cancelled?
    def cancelled?
      @data['cancelled'] == 'true'
    end

    # Allowed units ?
    # set of p1c, p2c, p5c, p10c, p20c, p50c, p1e, p2e
    #        b5e, b10e, b20e, b50e, b100e, b200e, b500e
    #        coins, notes
    # most probably "units" will be one (or two) of [coins, notes]
    def units
      @units ||= @data['units'][1..-2].split(',').map(&:to_sym)
    end

    # Units reported with counting problems, must be checked
    # set of units (see above)
    def flagged
      # [ p1c, p2c, ... ]
      @flagged ||= @data['flagged'][1..-2].split(',').map(&:to_sym)
    end

    # JSON object where keys are in set of [in, out, control, payment, collect]
    # and values are objects of type: { unit: count }
    def cash_info
      @cash_info ||= JSON.parse(@data['cash_info'], symbolize_names: true)
    end

    # JSON object of objects of type { unit: amount }
    def cash_sum
      @cash_sum ||= JSON.parse(@data['cash_sum'], symbolize_names: true)
    end

    # The raw data of table
    # {
    #   "before": { unit: { c: count, t: amount } }
    #   "after": { unit: { c: count, t: amount } }
    # }
    def raw
      @raw ||= JSON.parse(@data['raw'], symbolize_names: true, decimal_class: BigDecimal)
    end

    # The raw data, formatted as a table
    def table
      @data['table']
    end

    # The execution result as returned by the CashManager, format depends on the cash manager
    def result
      @result ||= JSON.parse(@data['result'])
    end

    # File checksum
    def checksum
      @data['checksum']
    end

  end
end
