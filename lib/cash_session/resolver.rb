module CashSession
  class Resolver
    def initialize(unit, count, total)
      @values = (unit == :coins ? COINS_CENTS : NOTES_CENTS).map { |v| BigDecimal(v / 100.0, 2) }.reverse
      @count = count
      @total = BigDecimal(total, 2)
      @solutions = []
    end

    def all
      try_values([], @count, @total, @values)
      @solutions # .uniq!
    end

    def try_values(solution, count, remainder, values)
      if count.zero? && remainder.zero?
        @solutions << solution
        return true
      end

      return false if count.negative? || remainder.negative? || values.empty?

      (0..values.length - 1).each do |i|
        v = values[i]
        try_values(solution + [v], count - 1, remainder - v, values[i..])
      end

      # if try_value(count, remainder, values[0])
      #   if try_values(solution + [values[0]], count - 1, remainder - values[0], values)
      #     try_values(solution + [values[0]], count - 1, remainder - values[0], values[1..])
      #   else
      #     try_values(solution, count, remainder, values[1..])
      #   end
      # else
      #   try_values(solution, count, remainder, values[1..])
      # end

      #
      # return [] unless try_value(count, remainder, values[0])
      #
      # solution = [values[0]]
      # count -= 1
      # remainder -= values[0]
      #
      # while count.positive?
      #   c = count
      #   values.each do |v|
      #     next unless try_value(count, remainder, v)
      #
      #     count -= 1
      #     remainder -= v
      #     solution << v
      #     break
      #   end
      #   break if count == c || (count.zero? && remainder.zero?)
      # end
      #
      # count.zero? && remainder.zero? ? solution : []
    end

    def try_value(count, remainder, value)
      r = remainder - value
      (r.zero? and count == 1) or r.positive?
    end
  end
end