require 'strscan'
require 'bigdecimal'
require 'time'
require 'json'

require "cash_session/version"
require "cash_session/units"
require "cash_session/reader"
require "cash_session/resolver"
require "cash_session/state"

module CashSession
  class Error < StandardError; end

  class NotEmpty < Error; end

  class Unsupported < Error; end

  class UnsupportedUnit < Error; end
end
